#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<sys/socket.h>

#include"inputThread.h"
#include"common_v4l2.h"

typedef unsigned int uint;

// Saves the information in data as a low-res PPM
static void save_ppm(uint i, uint xRes, uint yRes, size_t dataLength, char* data)
{
	FILE* fout;
	char out_name[256];
	sprintf(out_name, "pictures/out%04d.ppm", i);
	fout = fopen(out_name, "w");
	if (!fout)
	{
		perror("error: fopen");
		exit(EXIT_FAILURE);
	}
	
	fprintf(fout, "P6\n%d %d 255\n", xRes, yRes);
	fwrite(data, dataLength, 1, fout);
	fclose(fout);
}

void softReset()
{
	using namespace std::chrono_literals;
//	std::cout << "Resetting" << std::endl;
	scheduleInput(0x0ff0, 5);
	std::this_thread::sleep_for(5000ms);

	// Main menu
	for (int i=0; i<4; i++)
	{
		scheduleInput(0x0ffe, 5);
		std::this_thread::sleep_for(1500ms);
	}

	// Clicking through textboxes; happens faster.
	for (int i=0; i<12; i++)
	{
		scheduleInput(0x0ffe, 5);
		std::this_thread::sleep_for(1000ms);
	}

	std::this_thread::sleep_for(16500ms); // Celebi's entry animation
//	std::cout << "Done resetting" << std::endl;
}

// Celebi is normally green; therefore, on the first reset, find the greenest pixel and track it.
// Yes, this assumes that the camera doesn't move.
// Takes the picture size and the raw picture data, returns the pixel position as an int.
int calibrateCamera(int size, char* data)
{
	int greenest = 0;
	int greenestPixel = 0;
	int rgb[3];
	for (int i=0; i<size-3; i+=3)
	{
		rgb[0] = (int)data[i] & 0xFF; // red
		rgb[1] = (int)data[i+1] & 0xFF; // green
		rgb[2] = (int)data[i+2] & 0xFF; // blue

		// Find the "green-ness" of the pixel by subtracting the average from the green value
		int g = rgb[1] - ((rgb[0] + rgb[1] + rgb[2]) / 3);
		if (g > greenest)
		{
			greenestPixel = i;
			greenest = g;
		}
	}
	std::cout << "Greenest pixel found at x = " << (greenestPixel / 3) % 160 << ", y = " << (greenestPixel / 480) << std::endl;
	return greenestPixel;
}

// First time through: find the greenest pixel (Celebi's body).
// If on some reset the pixel is pink, that Celebi is Shiny and we should stop.
int main(int argc, char** argv)
{
	using namespace std::chrono_literals;
	const char* dev_name = "/dev/video2";
	uint xRes = 160, yRes = 120;
	CommonV4l2* commonv4l2 = new CommonV4l2(dev_name, xRes, yRes);
	commonv4l2->update_image();
	int size = commonv4l2->get_image_size();
	char* data = commonv4l2->get_image();
	int rgb[3];

	// Calibrate the camera
	int greenestPixel = calibrateCamera(size, data);
	std::thread* inputThread;
	startInputThread(inputThread);
	int attempts = 0;
	if (argc > 1) attempts = std::stoi(argv[1]);
	// Main loop
	while (true)
	{
		attempts++;
		std::cout << "Reset " << attempts << " ";
		int average = 0;

		// Take 4 consecutive pictures to ensure we're always using the latest one
		// Don't ask me why but if just one picture is taken then old pictures are
		// kept. It's a dirty hack, but it works.
		for (int i=0; i<4; i++)	commonv4l2->update_image()
		std::this_thread::sleep_for(100ms);
		char* data = commonv4l2->get_image();
		rgb[0] = (int)data[greenestPixel] & 0xFF;
		rgb[1] = (int)data[greenestPixel+1] & 0xFF;
		rgb[2] = (int)data[greenestPixel+2] & 0xFF;
		average = (rgb[0] + rgb[1] + rgb[2]) / 3;
		std::cout << "RGB: " << rgb[0] << "/" << rgb[1] << "/" << rgb[2] << std::endl;
		save_ppm(attempts, xRes, yRes, size, data);
		if (rgb[0] > 50 && rgb[0] > rgb[1]) // If the pixel is pink/magenta instead of green, the Celebi is shiny!
		{
			std::cout << "Shiny Celebi found in " << attempts << " resets" << std::endl;
			stopInputThread();
			while (true) {} // Hang the program indefinitely
		}
		else softReset();
	}
	// We should never get to this point but you never know
	delete commonv4l2;
	return 0;
}
