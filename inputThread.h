#include<cstdlib>
#include<thread>
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<netdb.h>


void scheduleInput(int input, int cycles);
void startInputThread(std::thread* inputLoop);
void stopInputThread();
