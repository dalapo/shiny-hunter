#include"inputThread.h"

/**
 * This thread handles sending UDP packets to the 3DS, which runs custom firmware with input redirection
 * to accept those packets as button inputs.
 */
int nextInput = 0xfff;
int numCycles = 0;
const int PORT = 4950;
const char* IP = "10.0.0.229";
int fd = 0;

void error(const char* msg)
{
	perror(msg);
	exit(1);
}

// Code copy-pasted from an online guide
int initSocket()
{
	int sockfd = 0;
	char buffer[256];
	struct sockaddr_in serv_addr;
	struct hostent* server;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	if (sockfd < 0)
	{
		error("ERROR opening socket");
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	if (inet_pton(AF_INET, IP, &serv_addr.sin_addr) <= 0)
	{
		std::cout << "Invalid address: Address not supported" << std::endl;
		exit(255);
	}

	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		std::cout << "Connection failed" << std::endl;
		exit(255);
	}
	fd = sockfd;
	return sockfd;
}

void sleep(int ms)
{
	usleep(1000 * ms);
}

/*
char* littleEndianIntToBytes(int i)
{
	char* c = new char[4];
	c[0] = (i & 0xFF);
	c[1] = (i >> 8) & 0xFF;
	c[2] = (i >> 16) & 0xFF;
	c[3] = (i >> 24) & 0xFF;
	return c;
}
*/

// Performs a little-endian conversion of integer i to four bytes which are stored in dest
void toLittleEndian(int i, char* dest)
{
	*dest = i & 0xFF;
	*(dest + 1) = (i >> 8) & 0xFF;
	*(dest + 2) = (i >> 16) & 0xFF;
	*(dest + 3) = (i >> 24) & 0xFF;
}

// Sends a button combination to the 3DS.
void sendButtonCombination(int socket, int buttons, int delay)
{
//	std::cerr << std::hex << "Sending button command " << buttons << std::endl;
	char* bytes = new char[20];
	toLittleEndian(buttons, bytes);
	toLittleEndian(0x20000000, bytes + 4); // Touch screen; default value
	toLittleEndian(0x007ff7ff, bytes + 8); // Circle pad; default value
	toLittleEndian(0x80800081, bytes + 12); // CPP; default value
	toLittleEndian(0x00000000, bytes + 16); // Power/Home; default value
	send(socket, bytes, delay, 0);
	delete[] bytes;
	bytes = NULL;
//	if (buttons != 0xfff) std::cerr << "Returning from send" << std::endl;
}

// Schedules an input to be repeated for a number of cycles.
// This is necessary to ensure the 3DS properly receives every packet.
void scheduleInput(int in, int cycles)
{
	nextInput = in;
	numCycles = cycles;
}

bool isLooping = true;
void doInputLoop(int socket)
{
	using namespace std::chrono_literals;
	while (isLooping)
	{
		if (numCycles-- > 0)
		{
	//		std::cout << "Sending button combination " << std::hex << nextInput << std::endl;
			sendButtonCombination(socket, nextInput, 20);
			std::this_thread::sleep_for(20ms);
		}
		else
		{
			// 0x0fff is "no input"
			sendButtonCombination(socket, 0x0fff, 20);
			std::this_thread::sleep_for(20ms);
		}
	}
}

void startInputThread(std::thread* inputLoop)
{
	int sockfd = initSocket();
	inputLoop = new std::thread(doInputLoop, sockfd);
	inputLoop->detach();
}

void stopInputThread()
{
	isLooping = false;
	numCycles = 0;
	shutdown(fd, SHUT_RDWR);
}
